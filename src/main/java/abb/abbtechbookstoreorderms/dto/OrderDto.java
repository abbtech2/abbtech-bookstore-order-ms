package abb.abbtechbookstoreorderms.dto;

import jakarta.persistence.Column;

import java.util.UUID;

public record OrderDto (Integer bookId,int quantity,Integer userId){

}
