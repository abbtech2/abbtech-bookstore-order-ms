package abb.abbtechbookstoreorderms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AbbtechBookstoreOrderMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbbtechBookstoreOrderMsApplication.class, args);
    }

}
