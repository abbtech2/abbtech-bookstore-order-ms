package abb.abbtechbookstoreorderms.repository;

import abb.abbtechbookstoreorderms.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderRepo extends JpaRepository<Order, UUID> {

}
