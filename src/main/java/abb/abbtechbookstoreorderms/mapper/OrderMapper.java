package abb.abbtechbookstoreorderms.mapper;

import abb.abbtechbookstoreorderms.dto.OrderDto;
import abb.abbtechbookstoreorderms.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    OrderDto mapOrderToDto(Order order);

    Order mapOrderDtoToEntity(OrderDto orderDto);

}
