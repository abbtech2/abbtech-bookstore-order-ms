package abb.abbtechbookstoreorderms.client;

import abb.abbtechbookstoreorderms.dto.NotificationDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient(value = "abb-tech-bookstore-notification",url = "${endpoints.abb-tech-bookstore-notification}")
public interface NotificationMs {

    @GetMapping(path = "/notification/{id}")
    NotificationDto getNotificationByID(@PathVariable UUID id);
}
