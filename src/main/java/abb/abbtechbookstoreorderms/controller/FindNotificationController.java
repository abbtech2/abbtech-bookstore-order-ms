package abb.abbtechbookstoreorderms.controller;

import abb.abbtechbookstoreorderms.dto.NotificationDto;
import abb.abbtechbookstoreorderms.service.FindNotificationService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "/notifications")
@RequiredArgsConstructor
@Tag(name = "Notification service",description = "My Notification  Controller")
@Slf4j
public class FindNotificationController {

    private final FindNotificationService notificationService;

    @GetMapping("/{id}")
    public ResponseEntity<NotificationDto> getNotification(@PathVariable UUID id){
        return ResponseEntity.ok(notificationService.findNotifications(id));
    }
}
