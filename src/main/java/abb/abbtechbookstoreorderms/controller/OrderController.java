package abb.abbtechbookstoreorderms.controller;

import abb.abbtechbookstoreorderms.dto.OrderDto;
import abb.abbtechbookstoreorderms.service.OrderService;
import abb.abbtechbookstoreorderms.service.impl.OrderServiceImpl;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/bookstore-order")
@RequiredArgsConstructor
@Tag(name = "Bookstore order",description = "My Bookstore Order Controller")
@Slf4j
public class OrderController {

    private final OrderService orderService;

    @PostMapping(path = "/save")
    public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto orderDto){
        OrderDto saverOrder = orderService.saveOrder(orderDto);
        return new ResponseEntity<>(saverOrder, HttpStatus.CREATED);
    }

    @PostMapping(path = "/update/{id}")
    public ResponseEntity<OrderDto> updateOrder(@PathVariable UUID id, @RequestBody OrderDto orderDto){
        OrderDto saverOrder = orderService.updateOrder(id,orderDto);
        return new ResponseEntity<>(saverOrder, HttpStatus.CREATED);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<OrderDto>> getAllOrders(){
        List<OrderDto> allOrders = orderService.getAkkOrders();
        return new ResponseEntity<>(allOrders, HttpStatus.OK);
    }
    @GetMapping(path = "/{id}")

    public ResponseEntity<OrderDto> getOrder(@PathVariable UUID id){
        Optional<OrderDto> order = orderService.getOrderById(id);
        OrderDto orderDto = order.get();
        return new ResponseEntity<>(orderDto, HttpStatus.OK);
    }
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Void> deleteOrder(@PathVariable UUID id){
        orderService.deleteOrder(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }



}
