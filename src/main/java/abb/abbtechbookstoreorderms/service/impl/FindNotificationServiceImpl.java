package abb.abbtechbookstoreorderms.service.impl;

import abb.abbtechbookstoreorderms.client.NotificationMs;
import abb.abbtechbookstoreorderms.dto.NotificationDto;
import abb.abbtechbookstoreorderms.service.FindNotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FindNotificationServiceImpl implements FindNotificationService {

    private final NotificationMs notificationMs;

    @Override
    public NotificationDto findNotifications(UUID id) {
        return notificationMs.getNotificationByID(id);
    }
}
