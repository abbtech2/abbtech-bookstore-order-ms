package abb.abbtechbookstoreorderms.service.impl;

import abb.abbtechbookstoreorderms.dto.OrderDto;
import abb.abbtechbookstoreorderms.entity.Order;
import abb.abbtechbookstoreorderms.mapper.OrderMapper;
import abb.abbtechbookstoreorderms.repository.OrderRepo;
import abb.abbtechbookstoreorderms.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepo orderRepo;
    private final OrderMapper orderMapper;

    public OrderDto saveOrder(OrderDto orderDto) {
        Order order = orderMapper.mapOrderDtoToEntity(orderDto);
        log.info("Saving order: {}", order);
        return orderMapper.mapOrderToDto(orderRepo.save(order));
    }

    @Override
    public List<OrderDto> getAkkOrders() {
        return orderRepo.findAll().stream()
                .map(orderMapper::mapOrderToDto)
                .toList();
    }

    @Override
    public Optional<OrderDto> getOrderById(UUID id) {
        return orderRepo.findById(id).map(orderMapper::mapOrderToDto);
    }

    @Override
    public OrderDto updateOrder(UUID id, OrderDto updatedOrder) {
        orderRepo.findById(id)
                .map(order -> {
                    order.setBookId(updatedOrder.bookId());
                    order.setQuantity(updatedOrder.quantity());
                    order.setUserId(updatedOrder.userId());
                    return orderRepo.save(order);
                }).orElseThrow(()-> new RuntimeException("Could not update"));
        return null;
    }

    @Override
    public void deleteOrder(UUID id) {
        orderRepo.findById(id).ifPresent(orderRepo::delete);
    }
}
