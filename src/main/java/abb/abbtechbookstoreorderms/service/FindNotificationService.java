package abb.abbtechbookstoreorderms.service;

import abb.abbtechbookstoreorderms.dto.NotificationDto;

import java.util.UUID;

public interface FindNotificationService {

    NotificationDto findNotifications(UUID id);
}
