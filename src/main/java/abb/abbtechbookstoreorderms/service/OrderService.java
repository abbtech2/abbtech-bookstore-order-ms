package abb.abbtechbookstoreorderms.service;

import abb.abbtechbookstoreorderms.dto.OrderDto;
import abb.abbtechbookstoreorderms.entity.Order;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderService {

    OrderDto saveOrder(OrderDto orderDto);

    List<OrderDto> getAkkOrders();

    Optional<OrderDto> getOrderById(UUID id);

    OrderDto updateOrder(UUID id, OrderDto updatedBook);

    void deleteOrder(UUID id);
}
